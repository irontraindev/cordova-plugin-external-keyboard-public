package org.apache.cordova.externalkeyboard;

import android.app.Activity;

import android.content.res.Configuration;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * cordova-plugin-external-keyboard
 * 외부 키보드 감지 플러그인
 */

public class ExternalKeyboard extends CordovaPlugin{
    /* 외부 키보드 사용 여부 반환 값 */
    static boolean isUsed = false;
    /* javascript 실행 부분 */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        if (action.equals("isExternalKeyboardAttached")) {
            /* 외부 키보드 검사 */
            isUsed = this.isExternalKeyboardAttached();
            /* 외부 키보드 사용여부 반환 */
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, isUsed));
            return true;
        }
        return false;
    }
    /* 외부 키보드 검사 */
    private boolean isExternalKeyboardAttached() {
        Configuration config = cordova.getActivity().getResources().getConfiguration();
        return !(config.keyboard == Configuration.KEYBOARD_NOKEYS
                || config.hardKeyboardHidden == Configuration.KEYBOARDHIDDEN_YES);
    }


}
